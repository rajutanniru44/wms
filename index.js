'use strict';

const server = require('./server'),
    cors = require('./conf').cors,
	host = process.env.HOST || '0.0.0.0',
	port = process.env.PORT || 8080
    
// Start the server with the host and port specified as passed-in arguments
module.exports = server.start(host, port, cors);
