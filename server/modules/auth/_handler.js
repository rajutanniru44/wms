'use strict'

const jwt   = require('jsonwebtoken');
const randtoken = require('rand-token') 
const secret = "$iv@r@jendr@pr@$ad";
const {searchUser} = require('../users')
const crypto = require('crypto');                        
const md5 = (password) => {
    return crypto.createHash('md5').update(password).digest('hex');
}

// bring your own validation function
const validate = async function (decoded, request) {    
    try {
        // do your checks to see if the person is valid
        let data = decoded.data
        if (data.hasOwnProperty('_id') && data.hasOwnProperty('username')) {
            // do stuff            
        return { isValid: true };
        }
        else {
        return { isValid: false };
        }
    } catch (error) {
        
    }

};

const jwt_options =  { key: secret,          // Never Share your secret key
    validate: validate,            // validate function defined above
    verifyOptions: { algorithms: [ 'HS256' ] } // pick a strong algorithm
}

const login =   async (request, h) => {
    try {       
        let userObj = await searchUser({username:request.payload.username})
        if(userObj) {   
            let md5Password = md5(request.payload.password )   
            if(md5Password === userObj.password) {
                //   generate refreshToken
                var refreshToken = randtoken.uid(256)                 
                let dt = new Date()
                dt.setMonth(dt.getMonth()+1)
                userObj.refresh_token_expiry_time =     new Date(dt)
                userObj.refresh_token = refreshToken;
                await userObj.save()
                // const token = JWT.sign(obj, secret);
                const token = jwt.sign({
                    exp: Math.floor(Date.now() / 1000) + (2*60 * 60), // 2 hour
                    data: { _id:userObj._id, username: userObj.username, position: userObj.position }
                  }, secret);
                return {error:"", message:"Welcome User!", result: {token: token, refresh_token:refreshToken, user: userObj}}
            } else {
                return {error:"", message:"Invalid user credentials", result:"" }
            }
               
        } else {
            return {error:"", message:"Invalid user credentials", result:"" }   
        }
    } catch (error) {
        return {error: error.message}
    }
}

const generate_token_from_refresh_token = async (request, h) => {
    let refresh_token =  request.payload.refresh_token;
    let username =  request.payload.username;
    let user = await searchUser({refresh_token: refresh_token, username: username});
    // console.log(user)
    if(user) {
        let currentDate = new Date()
        if(currentDate > user.refresh_token_expiry_time) {
            return {error:"Token Expired", message:"Refresh token expired", result:"" }
        } else {
            const token = jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60), // 1 hour
                data: { _id: user._id, username: user.username, position: user.position}
              }, secret);
            return {error:"", message:"Welcome User!", result: {token: token}}
        }
    } else {
        return {error:"", message:"Invalid user credentials", result:"" }
    }
}

module.exports = {  
    jwt_options,  
    login,
    generate_token_from_refresh_token
}
