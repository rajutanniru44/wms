
'use strict';
const user = require('../users')
const { jwt_options, login, generate_token_from_refresh_token } = require('./_handler')
const joi = require('joi')
// Schema
const loginSchema = joi.object().keys(
    {
        username: joi
            .string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),
        password: joi
            .string()
            .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,12}$/)
            .required()
            .description("Minimum eight and maximum 12 characters, at least one uppercase letter, one lowercase letter, one number and one special character"),
    });

const _routes = [{
    method: 'GET',
    path: '/api/v1/',
    config: {
        handler: (request, h) => { return 'Welcome Guest! this is default end point. \nOne should login first and provide token in "_handlerentication" header to access other api end points' },
        description: 'home',
        notes: 'Returns welcome message',
        tags: ['api', 'v1', 'application'],
        auth: false
    },
},
{
    method: 'POST',
    path: '/api/v1/auth/login',
    config: {
        handler: login,
        description: 'login',
        notes: 'Returns token and refresh token after success',
        tags: ['api', 'v1', 'application'],
        auth: false,
        validate: {
            payload: loginSchema
        }
    },
},
{
    method: 'POST',
    path: '/api/v1/auth/create-new-token',
    config: {
        handler: generate_token_from_refresh_token,
        validate: {
            payload: {
                username: joi
                    .string()
                    .alphanum()
                    .min(3)
                    .max(30)
                    .required(),
                refresh_token: joi
                    .string()
                    .required()
                    .description('refresh token to identify user')
            }
        },
        description: 'generate new token from refresh_token',
        notes: 'Returns token',
        tags: ['api', 'v1', 'application'],
        auth: false
    },
},
    // {
    //     method: 'PUT',
    //     path: '/api/v1/_handler/reset-user-password/{temp_token}',
    //     config: {
    //         handler: user.modify,
    //         description: 'Reset user password',
    //         notes: [],
    //         tags: ['api', 'v1', 'application'],
    //         validate: {
    //             payload: {
    //                 new_password: joi
    //                     .string()
    //                     .required()
    //                     .description('new password'),
    //                 confirm_new_password: joi
    //                     .string()
    //                     .required()
    //                     .description('confirm new password')                
    //             },
    //             query: {
    //                 temp_token: joi
    //                     .string()
    //                     .required()
    //                     .description('temp token to identify user')
    //             }
    //         }
    //     },        
    // },
]

module.exports = { _routes, jwt_options }