'use strict'

const mongoose = require('mongoose'),
    url = require('../../../conf').db.url;

// connect    
mongoose.connect(url, { autoIndex: false });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!

});

var Schema = mongoose.Schema;

var schema = new Schema({
    username:  {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    },
    position: {
      type: String,
      required: true
    },  
    creation_time: { type: Date, default: Date.now },
    modification_time: { type: Date, default: Date.now },
    refresh_token: { type: String},
    refresh_token_expiry_time: { type: Date },
});

module.exports = db.model('User', schema);