'use strict';
const _handler = require('./_handler')
const joi = require('joi')
// Schema
const NewUserSchema = joi.object().keys(
    {
        username: joi
            .string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),
        password: joi
            .string()
            .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,12}$/)
            .required()
            .description("Minimum eight and maximum 12 characters, at least one uppercase letter, one lowercase letter, one number and one special character"),
        position: joi
            .string()
            .required()
            .valid('admin', 'branch_manager', 'marketing_person', 'customer')
            .description('position')
            .example('admin'),
    }
)

const UpdateUserSchema = joi.object().keys(
    {
        _id: joi
            .string()
            .required(),
        username: joi
            .string()
            .alphanum()
            .min(3)
            .max(30)
            .required(),
        change_password: joi.boolean().required(),    
        password: joi
            .string()
            .description("Minimum eight and maximum 12 characters, at least one uppercase letter, one lowercase letter, one number and one special character"),
        position: joi
            .string()
            .required()
            .valid('admin', 'branch_manager', 'marketing_person', 'customer')
            .description('position')
            .example('admin'),
        creation_time: joi
            .date()
            .required(),
        modification_time: joi
            .date()
            .required(),
        __v: joi
            .number()
            .required(),  
        refresh_token: joi
            .string(),
        refresh_token_expiry_time: joi
            .date()
    }
)

// password_confirmation: Joi
//     .any()
//     .valid(Joi.ref('password'))
//     .required()
//     .options(
//         { language: 
//             { any:
//                  { allowOnly: 'must match password' } 
//                 } 
//             }
//     )

const _routes = [{
    method: 'GET',
    path: '/api/v1/users',
    config: {
        handler: _handler.find,
        description: 'Get All Users',
        notes: 'Returns Users Array',
        tags: ['api', 'v1', 'users'],
        auth: 'jwt'
    },        
},
{
    method: 'GET',
    path: '/api/v1/users/{_id}',
    config: {
        handler: _handler.findByID,
        description: 'Get User By ID',
        notes: 'Returns User',
        tags: ['api', 'v1', 'users'],
        validate: {
            params: {
                _id: joi
                    .string()
                    .required()
                    .description('_id field of user')
            }
        }
    },        
},
{
    method: 'PUT',
    path: '/api/v1/users',
    config: {
        handler: _handler.modify,
        description: 'Modify User',
        notes: 'Returns User',
        tags: ['api', 'v1', 'users'],
        validate: {
            payload: UpdateUserSchema
        }
    },        
},
{
    method: 'POST',
    path: '/api/v1/users',
    config: {
        handler: _handler.add,
        description: 'Add New User',
        notes: 'Returns User',
        tags: ['api', 'v1', 'users'],
        validate: {
            payload: NewUserSchema
        }
    },        
},
{
    method: 'DELETE',
    path: '/api/v1/users/{_id}',
    config: {
        handler: _handler.remove,
        description: 'Remove User',
        notes: 'Delete user',
        tags: ['api', 'v1', 'users'],
        validate: {
            params: {
                _id: joi
                    .string()
                    .required()
                    .description('_id field of user')
            }
        }
    },        
},   
]

const searchUser = _handler.searchUser
module.exports = {_routes, searchUser}