'use strict'

const User = require('./user.model')
const ObjectId = require('mongoose').Types.ObjectId
const joi = require('joi')
const crypto = require('crypto');
const md5 = (password) => {
    return crypto.createHash('md5').update(password).digest('hex');
}
const PasswordSchema = joi
    .string()
    .required()
    .regex(/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&]{8,12}$/)
    .description("Minimum eight and maximum 12 characters, at least one uppercase letter, one lowercase letter, one number and one special character");


const _handler = {
    find: async (request, h) => {
        try {
            let condition = {}
            let result = await User.find(condition)
            return { error: "", message: "", result: result }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }
    },
    findByID: async (request, h) => {
        try {
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await User.findById(_id);
                return { error: "", message: "", result: result }
            } else {
                return { "error": "Bad Request", "message": "Invalid request payload input", result: "" }
            }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }
    },
    searchUser: async (filter) => {
        try {
             console.log('u r in searchUser')
             console.log(filter)
            if (filter) {
                let user = await User.findOne(filter)
                return user;
                // let result = await User.findById(_id);
                // return { error: "", message: "", result: result }
            } else {
                return { "error": "Bad Request", "message": "Invalid request payload input", result: "" }
            }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }
    },
    add: async (request, h) => {
        try {
            // check for duplicate
            let user = await User.findOne({ username: request.payload.username })
            if (user) {
                return { error: "Duplicate Error", message: "Duplicate record exists", result: "" }
            } else {
                request.payload.password = md5(request.payload.password)
                let u1 = new User(request.payload);
                let result = await u1.save();
                return { error: "", message: "", result: result }
            }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }
    },
    modify: async (request, h) => {
        try {
            if (ObjectId.isValid(request.payload._id)) {
                // check for duplicate
                let user = await User.findOne({ username: request.payload.username }).ne('_id', request.payload._id)
                if (user) {
                    return { error: "Duplicate Error", message: "Duplicate record exists", result: "" }
                } else {
                    if (request.payload.change_password) {
                        // change password
                        delete request.payload.change_password
                        if (joi.validate(request.payload.password, PasswordSchema)) {
                            request.payload.password = md5(request.payload.password)
                        } else {
                            return { "error": "Bad Request", "message": "Invalid request payload input", result: "" }
                        }
                    } else {
                        // do not update password                        
                        delete request.payload.password
                    }

                    try {
                        // delete unnecessary properties
                        delete request.payload.change_password
                        delete request.payload.creation_time;
                        delete request.payload.__v;
                        delete request.payload.refresh_token;
                        delete request.payload.refresh_token_expiry_time;

                        // also update modify time
                        request.payload.modification_time = new Date()
                    } catch (error) {
                        // do nothing
                        console.log(error)
                    }

                    let result = await User.findByIdAndUpdate({ _id: request.payload._id }, request.payload, { new: true })
                    return { error: "", message: "", result: result }
                }
            } else {
                return { "error": "Bad Request", "message": "Invalid request payload input", result: "" }
            }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }

    },
    remove: async (request, h) => {
        try {
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await User.remove({ _id: _id });
                return { error: "", message: "", result: result };
            } else {
                resolve({ "error": "Bad Request", "message": "Invalid request payload input", result: "" })
            }
        } catch (error) {
            return { error: error.message, message: "", result: "" }
        }
    }
}

module.exports = _handler