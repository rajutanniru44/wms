'use strict';
const { ItemHandler, IncomingStockHandler, OutgoingStockHandler, FeedbackHandler } = require('./_handler')
const { FeedbackSchema, ItemSchema, IncomingStockSchema, OutgoingStockSchema } = require('./schemas')

// _routes
const _routes = [
    
// item routes    
{
    method: 'GET',
    path: '/api/v1/items',
    config: {
        handler: ItemHandler.find,
        description: 'Get All Items',
        notes: 'Returns Items Array',
        tags: ['api', 'v1', 'items']
    },
},
{
    method: 'POST',
    path: '/api/v1/items',
    config: {
        handler: ItemHandler.add,
        description: 'Add Item',
        notes: 'Returns Items Array',
        tags: ['api', 'v1', 'items'],
        validate: {
            payload: ItemSchema.NewItem
        }
    },
},
{
    method: 'PUT',
    path: '/api/v1/items',
    config: {
        handler: ItemHandler.modify,
        description: 'Save Item',
        notes: 'Returns Items Array',
        tags: ['api', 'v1', 'items'],
        validate: {
            payload: ItemSchema.UpdateItem
        }
    },
},
{
    method: 'DELETE',
    path: '/api/v1/items/{_id}',
    config: {
        handler: ItemHandler.remove,
        description: 'Delete Item',
        notes: 'Returns Items Array',
        tags: ['api', 'v1', 'items'],
        validate: {
            params: ItemSchema.LookForId
        }
    },
},

// incoming-stocks routes
{
    method: 'GET',
    path: '/api/v1/incoming-stocks',
    config: {
        handler: IncomingStockHandler.find,
        description: 'Get All Incoming Stocks',
        notes: 'Returns Incoming Stocks Array',
        tags: ['api', 'v1', 'Incoming Stocks']
    },
},
{
    method: 'POST',
    path: '/api/v1/incoming-stocks',
    config: {
        handler: IncomingStockHandler.add,
        description: 'Add Incoming Stock',
        notes: 'Returns Incoming Stocks Array',
        tags: ['api', 'v1', 'Incoming Stocks'],
        validate : {
            payload : IncomingStockSchema.NewIncomingStock
        }
    },
},
{
    method: 'PUT',
    path: '/api/v1/incoming-stocks',
    config: {
        handler: IncomingStockHandler.modify,
        description: 'Save Incoming Stock',
        notes: 'Returns Incoming Stocks Array',
        tags: ['api', 'v1', 'Incoming Stocks'],
        validate : {
            payload : IncomingStockSchema.UpdateIncomingStock
        }
    },
},
{
    method: 'DELETE',
    path: '/api/v1/incoming-stocks/{_id}',
    config: {
        handler: IncomingStockHandler.remove,
        description: 'Delete Incoming Stock',
        notes: 'Returns Incoming Stocks Array',
        tags: ['api', 'v1', 'Incoming Stocks'],
        validate : {
            params : IncomingStockSchema.LookForId
        }
    },
},

// outgoing-stocks routes
{
    method: 'GET',
    path: '/api/v1/outgoing-stocks',
    config: {
        handler: OutgoingStockHandler.find,
        description: 'Get All Outgoing Stocks',
        notes: 'Returns Outgoing Stocks Array',
        tags: ['api', 'v1', 'Outgoing Stocks']
    },
},
{
    method: 'POST',
    path: '/api/v1/outgoing-stocks',
    config: {
        handler: OutgoingStockHandler.add,
        description: 'Add Outgoing Stock',
        notes: 'Returns Outgoing Stocks Array',
        tags: ['api', 'v1', 'Outgoing Stocks'],
        validate : {
            payload : OutgoingStockSchema.NewOutgoingStock
        }
    },
},
{
    method: 'PUT',
    path: '/api/v1/outgoing-stocks',
    config: {
        handler: OutgoingStockHandler.modify,
        description: 'Save Outgoing Stock',
        notes: 'Returns Outgoing Stocks Array',
        tags: ['api', 'v1', 'Outgoing Stocks'],
        validate : {
            payload : OutgoingStockSchema.UpdateOutgoingStock
        }
    },
},
{
    method: 'DELETE',
    path: '/api/v1/outgoing-stocks/{_id}',
    config: {
        handler: OutgoingStockHandler.remove,
        description: 'Delete Outgoing Stock',
        notes: 'Returns Outgoing Stocks Array',
        tags: ['api', 'v1', 'Outgoing Stocks'],
        validate : {
            params : OutgoingStockSchema.LookForId
        }
    },
},

// feedback routes
{
    method: 'GET',
    path: '/api/v1/feedback',
    config: {
        handler: FeedbackHandler.find,
        description: 'Get All Feedback',
        notes: 'Returns Feedback Array',
        tags: ['api', 'v1', 'Feedback'],
    },
},
{
    method: 'POST',
    path: '/api/v1/feedback',
    config: {
        handler: FeedbackHandler.add,
        description: 'Add Feedback',
        notes: 'Save Feedback',
        tags: ['api', 'v1', 'Feedback'],
        validate: {
            payload: FeedbackSchema.NewFeedback
        }
    },
},
{
    method: 'PUT',
    path: '/api/v1/feedback',
    config: {
        handler: FeedbackHandler.modify,
        description: 'Update Feedback',
        notes: 'Update feedback',
        tags: ['api', 'v1', 'Feedback'],
        validate: {
            payload: FeedbackSchema.UpdateFeedback
        }
    },
},
{
    method: 'DELETE',
    path: '/api/v1/feedback/{_id}',
    config: {
        handler: FeedbackHandler.remove,
        description: 'Remove feedback',
        notes: 'Delete feedback',
        tags: ['api', 'v1', 'feedback'],
        validate: {
            params: FeedbackSchema.LookForId
        }
    },
},
]

// export
module.exports = {_routes}
