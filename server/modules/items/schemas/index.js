'use strict'

const joi = require('joi')

// FeedbackSchema
var FeedbackSchema = {
    NewFeedback: joi.object().keys(
        {
            feedback_type: joi
                .string()
                .required()
                .valid('general', 'against_transaction')
                .example('general'),
            category: joi
                .string(),
            feedback: joi
                .string()
                .required(),
            order_id: joi
                .when('feedback_type', { is: 'against_transaction', then: joi.string().required() }),
            fbimages: joi.array().items(joi.string()),
            fbnotes: joi.array().items(joi.string()),
        }
    ),
    UpdateFeedback: joi.object().keys(
        {
            _id: joi
                .string()
                .required(),
            feedback_type: joi
                .string()
                .required()
                .valid('general', 'against_transaction')
                .example('general'),
            category: joi
                .string(),
            feedback: joi
                .string()
                .required(),
            order_id: joi
                .when('feedback_type', { is: 'against_transaction', then: joi.string().required() }),
            fbimages: joi.array().items(joi.string()),
            fbnotes: joi.array().items(joi.string()),
            creation_time: joi
                .date(),
            modification_time: joi
                .date(),
            __v: joi
                .number()
        }
    ),
    LookForId: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required')
        }
    )
}

// ItemSchema
var ItemSchema = {
    NewItem: joi.object().keys(
        {
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity: joi
                .string()
                .required(),
            date_recieved: joi
                .date()
                .required(),
            images: joi.array().items(joi.string()),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required()
        }
    ),
    UpdateItem: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required'),
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity: joi
                .string()
                .required(),
            date_recieved: joi
                .date()
                .required(),
            images: joi.array().items(joi.string()),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required(),
            creation_time: joi.date(),
            modification_time: joi.date(),
            __v: joi.number(),
        }
    ),
    LookForId: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required')
        }
    )
}

// IncomingStockSchema
var IncomingStockSchema = {
    NewIncomingStock: joi.object().keys(
        {
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity_in: joi
                .number()
                .required(),
            date_recieved: joi
                .date()
                .required(),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required()
        }
    ),
    UpdateIncomingStock: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required'),
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity_in: joi
                .number()
                .required(),
            date_recieved: joi
                .date()
                .required(),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required(),
            creation_time: joi.date(),
            modification_time: joi.date(),
            __v: joi.number(),
        }
    ),
    LookForId: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required')
        }
    )
}

// OutgoingStockSchema
var OutgoingStockSchema = {
    NewOutgoingStock: joi.object().keys(
        {
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity_out: joi
                .number()
                .required(),
            date_released: joi
                .date()
                .required(),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required()
        }
    ),
    UpdateOutgoingStock: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required'),
            item_code: joi
                .string()
                .required(),
            category: joi
                .string()
                .required(),
            description: joi
                .string()
                .required(),
            unit: joi
                .string()
                .required(),
            quantity_out: joi
                .number()
                .required(),
            date_released: joi
                .date()
                .required(),
            notes: joi.string(),
            expiry_date: joi
                .date()
                .required(),
            creation_time: joi.date(),
            modification_time: joi.date(),
            __v: joi.number(),
        }
    ),
    LookForId: joi.object().keys(
        {
            _id: joi
                .string()
                .required()
                .description('_id field is required')
        }
    )
}

module.exports = { FeedbackSchema, ItemSchema, IncomingStockSchema, OutgoingStockSchema }