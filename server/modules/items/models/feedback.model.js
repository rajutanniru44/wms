'use strict'

const mongoose = require('mongoose'),
url = require('../../../../conf').db.url;

// connect    
mongoose.connect(url, { autoIndex: false });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!

});

var Schema = mongoose.Schema;

var schema = new Schema({
    feeback_type: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    feedback: {
        type: String,
        required: true
    },
    order_id: {
        type: Number
    },
    fbimages: { type: Array },
    fbnotes: { type: String },
    creation_time: { type: Date, default: Date.now },
    modification_time: { type: Date, default: Date.now }
});

module.exports = db.model('Feedback', schema);