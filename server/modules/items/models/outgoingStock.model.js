'use strict'

const mongoose = require('mongoose'),
url = require('../../../../conf').db.url;

// connect    
mongoose.connect(url, { autoIndex: false });
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
    // we're connected!

});

var Schema = mongoose.Schema;

var schema = new Schema({
    item_code: {
        type: String,
        required: true
    },
    category: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    unit: {
        type: String,
        required: true
    },
    quantity_out: {
        type: Number,
        required: true
    },
    creation_time: { type: Date, default: Date.now },
    modification_time: { type: Date, default: Date.now },
    date_released: { type: Date, default: Date.now, required: true },
    notes: { type: String },
    expiry_date: { type: Date, required: true },
});

module.exports = db.model('outgoingStock', schema);