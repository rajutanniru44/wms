'use strict'

const Item = require('./models/item.model')
const IncomingStock = require('./models/incomingStock.model')
const OutgoingStock = require('./models/outgoingStock.model')
const Feedback = require('./models/feedback.model')
const ObjectId = require('mongoose').Types.ObjectId
// const joi = require('joi')

const ItemHandler = {
    find: async (request, h) => {        
        try {
            let condition = {}
            let result = await Item.find(condition)
            return {error:"", message:"", result:result}
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    findByID: async (request, h) => {        
        try {                
            
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    add: async (request, h) => {        
        try {
            // check for duplicate
            let duplicate = await Item.findOne({item_code: request.payload.item_code})
            if(duplicate) {
               return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
            } else {
                let u1 = new Item(request.payload);
                let result = await u1.save();
                return {error:"", message:"", result: result}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    modify: async (request, h) => {        
        try {
            if (ObjectId.isValid(request.payload._id)) {
                // check for duplicate
                let duplicate = await Item.findOne({item_code: request.payload.item_code}).ne('_id', request.payload._id)                
                if(duplicate) {
                    return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
                } else {                                                            
                    try {
                        // delete unnecessary properties                        
                        delete request.payload.creation_time;                    
                        delete request.payload.__v;                       
                        // update modify time
                        request.payload.modification_time = new Date()
                    } catch (error) {
                        throw Error('Unknown Error')
                    }
                    let result = await Item.findByIdAndUpdate( { _id:request.payload._id }, request.payload, {new: true} )   
                    return {error:"", message:"", result: result}
                }
            } else {
                return {"error": "Bad Request", "message": "Invalid request payload input", result:""}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    remove: async (request, h) => {                  
        try {                
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await Item.remove({ _id: _id});
                return {error:"", message:"", result: result};
            } else {
                resolve ( {"error": "Bad Request", "message": "Invalid request payload input", result:""} )
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    }
}

// IncomingStock
const IncomingStockHandler = {
    find: async (request, h) => {        
        try {
            let condition = {}
            let result = await IncomingStock.find(condition)
            return {error:"", message:"", result:result}
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    findByID: async (request, h) => {        
        try {                
            
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    add: async (request, h) => {        
        try {
            // check for duplicate
            // let duplicate = await Item.findOne({item_code: request.payload.item_code})
            let duplicate = false;
            if(duplicate) {
               return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
            } else {
                let u1 = new IncomingStock(request.payload);
                let result = await u1.save();
                return {error:"", message:"", result: result}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    modify: async (request, h) => {        
        try {
            if (ObjectId.isValid(request.payload._id)) {
                // check for duplicate
                // let duplicate = await Item.findOne({item_code: request.payload.item_code}).ne('_id', request.payload._id)                
                let duplicate = false;
                if(duplicate) {
                    return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
                } else {                                                            
                    try {
                        // delete unnecessary properties
                        delete request.payload.creation_time;                    
                        delete request.payload.__v;                       
                        // update modify time
                        request.payload.modification_time = new Date()
                    } catch (error) {
                        throw Error('Unknown Error')
                    }
                    let result = await IncomingStock.findByIdAndUpdate( { _id:request.payload._id }, request.payload, {new: true} )   
                    return {error:"", message:"", result: result}
                }
            } else {
                return {"error": "Bad Request", "message": "Invalid request payload input", result:""}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    remove: async (request, h) => {                  
        try {                
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await IncomingStock.remove({ _id: _id});
                return {error:"", message:"", result: result};
            } else {
                resolve ( {"error": "Bad Request", "message": "Invalid request payload input", result:""} )
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    }
}

// OutgoingStock
const OutgoingStockHandler = {
    find: async (request, h) => {        
        try {
            let condition = {}
            let result = await OutgoingStock.find(condition)
            return {error:"", message:"", result:result}
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    findByID: async (request, h) => {        
        try {                
            
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    add: async (request, h) => {        
        try {
            // check for duplicate
            // let duplicate = await Item.findOne({item_code: request.payload.item_code})
            let duplicate = false;
            if(duplicate) {
               return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
            } else {
                let u1 = new OutgoingStock(request.payload);
                let result = await u1.save();
                return {error:"", message:"", result: result}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    modify: async (request, h) => {        
        try {
            if (ObjectId.isValid(request.payload._id)) {
                // check for duplicate
                // let duplicate = await Item.findOne({item_code: request.payload.item_code}).ne('_id', request.payload._id)                
                let duplicate = false;
                if(duplicate) {
                    return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
                } else {                                                            
                    try {
                        // delete unnecessary properties
                        delete request.payload.creation_time;                    
                        delete request.payload.__v;                       
                        // update modify time
                        request.payload.modification_time = new Date()
                    } catch (error) {
                        throw Error('Unknown Error')
                    }
                    let result = await OutgoingStock.findByIdAndUpdate( { _id:request.payload._id }, request.payload, {new: true} )   
                    return {error:"", message:"", result: result}
                }
            } else {
                return {"error": "Bad Request", "message": "Invalid request payload input", result:""}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    remove: async (request, h) => {                  
        try {                
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await OutgoingStock.remove({ _id: _id});
                return {error:"", message:"", result: result};
            } else {
                resolve ( {"error": "Bad Request", "message": "Invalid request payload input", result:""} )
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    }
}

// Feedback
const FeedbackHandler = {
    find: async (request, h) => {        
        try {
            let condition = {}
            let result = await Feedback.find(condition)
            return {error:"", message:"", result:result}
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    findByID: async (request, h) => {        
        try {                
            
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    add: async (request, h) => {        
        try {
            // check for duplicate
            // let duplicate = await Item.findOne({item_code: request.payload.item_code})
            let duplicate = false;
            if(duplicate) {
               return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
            } else {
                let u1 = new Feedback(request.payload);
                let result = await u1.save();
                return {error:"", message:"", result: result}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    modify: async (request, h) => {        
        try {
            if (ObjectId.isValid(request.payload._id)) {
                // check for duplicate
                // let duplicate = await Item.findOne({item_code: request.payload.item_code}).ne('_id', request.payload._id)                
                let duplicate = false;
                if(duplicate) {
                    return {error:"Duplicate Error", message:"Duplicate record exists", result:""}
                } else {                                                            
                    try {
                        // delete unnecessary properties
                        delete request.payload.creation_time;                    
                        delete request.payload.__v;                       
                    } catch (error) {
                        throw Error('Unknown Error')
                    }
                    let result = await Feedback.findByIdAndUpdate( { _id:request.payload._id }, request.payload, {new: true} )   
                    return {error:"", message:"", result: result}
                }
            } else {
                return {"error": "Bad Request", "message": "Invalid request payload input", result:""}
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    },
    remove: async (request, h) => {                  
        try {                
            const { _id: _id } = request.params;
            if (ObjectId.isValid(_id)) {
                let result = await Feedback.remove({ _id: _id});
                return {error:"", message:"", result: result};
            } else {
                resolve ( {"error": "Bad Request", "message": "Invalid request payload input", result:""} )
            }
        } catch (error) {
            return {error:error.message, message:"", result: ""}
        }
    }
}
module.exports = {ItemHandler, IncomingStockHandler, OutgoingStockHandler, FeedbackHandler}