'use strict';

const Hapi = require('hapi')
const plugins = require('./plugins')
const routes = require('./init/routes')
const {jwt_options} = require('./modules/auth')
const Boom = require('boom')

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

const start = async (host, port, cors) => { 
    const server = Hapi.server({
        port: port,
        host: host,
        routes: {
            cors: cors,
            validate: {
                failAction: async (request, h, err) => { 
                    if (host === 'localhost') {
                        throw err
                    } else {
                        throw Boom.badRequest(`Invalid request payload input`);
                    }

                 }
            }
        }
    });

    await server.register(plugins);
    server.auth.strategy('jwt', 'jwt', jwt_options);
    server.auth.default('jwt');
    server.route(routes(server));
    await server.start();    
    console.log(`Server running at: ${server.info.uri}`);

}



module.exports = {
	start
};






// server.route({
//     method: 'GET',
//     path: '/',
//     handler: (request, h) => {

//         return 'Hello, world!';
//     }
// });

// server.route({
//     method: 'GET',
//     path: '/{name}',
//     handler: (request, h) => {
//         // request.logger.info('In handler %s', request.path);
//         return 'Hello, ' + encodeURIComponent(request.params.name) + '!';
//     }
// });


// const init = async () => {
    // await server.register({
    //     plugin: require('hapi-pino'),
    //     options: {
    //         prettyPrint: false,
    //         logEvents: ['response']
    //     }
    // });

//     await server.start();    
//     console.log(`Server running at: ${server.info.uri}`);
// };

// basicAuthValidation = require('./modules/auth').basicAuth;

// const start = (host, port) => {
// 	return new Promise((resolve, reject) => {

// 		// Create the server
// 		const server = new Hapi.Server({host,port});

// 		// Register all the plugins
// 		server.register(plugins, (err) => {
// 			if (err) {
// 				console.error(err);
// 				return reject(err);
// 			}

// 			// Basic HTTP auth - the 3rd param is `true`, which will turn on validation by default for all routes, unless specified specifically on the route itself
// 			// server.auth.strategy('basic', 'basic', true, { validateFunc: basicAuthValidation });

// 			// Initialize routes
// 			server.route(routes(server));

// 			// Start accepting requests
// 			server.start( (err) => {
// 				if (err) {
// 					console.error(err);
// 					return reject(err);
// 				}

// 				// Server started successfully - register routes
// 				console.log(`Server running at: ${server.info.uri}`);
// 				resolve();
// 			});

// 			server.on('request-error', (req, err) => {
// 				console.error(err);
// 			});
// 		});
// 	});
// };