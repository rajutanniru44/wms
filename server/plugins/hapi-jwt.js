'use strict'

const Hoek = require('hapi-auth-jwt'),
    pkg = require('../../package.json')

module.exports = {
    plugin : Hoek,
    pkg : pkg,
    options: {
        name: 'jwt'
    }
}

