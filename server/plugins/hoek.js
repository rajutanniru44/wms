'use strict'

const Hoek = require('hoek'),
    pkg = require('../../package.json')

module.exports = {
    plugin : Hoek,
    pkg : pkg
}
