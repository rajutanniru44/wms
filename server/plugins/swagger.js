'use strict';

// See: https://github.com/glennjones/hapi-swagger

const pkg = require('../../package.json');
const HapiSwagger = require('hapi-swagger');

const swaggerOptions = {
    info: {
        title: 'WMS documentation',
        description: `
This API is for WMS project.

To see all routes, [click here](/documentation).

To see V1 routes only, [click here](/documentation?tags=v1).

To see V2 routes only, [click here](/documentation?tags=v2).

To view the swagger.json, [click here](/swagger.json).
            `,
        // Get the version from package.json
        version: pkg.version,
        contact: {
            name: 'Siva R Prasad',
            url: 'http://localhost'
        },
        license: {
            // Get the license from package.json
            name: pkg.license
        }
    },
    basePath: '/api/'
};


module.exports = {
    plugin: HapiSwagger,
    options: swaggerOptions
}