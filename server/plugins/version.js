'use strict';

// See: https://github.com/p-meier/hapi-api-version

module.exports = {
	plugin: require('hapi-api-version'),
	options: {
		basePath: '/api/',
		validVersions: [1,2],
		defaultVersion: 1,
		vendorName: 'sivarpc'
	}
};