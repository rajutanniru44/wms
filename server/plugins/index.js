'use strict';

const Pino = require('hapi-pino'),
    Inert = require('inert'),
    Vision = require('vision'),
    Swagger = require('./swagger'),
    Version = require('./version'),
    HapiJWT = require('hapi-auth-jwt2')

module.exports = [Inert, Vision, Swagger, Version, HapiJWT]