'use strict';
const joi = require('joi')
const Hoek = require('hoek')

const allRoutes = []
Hoek.merge(allRoutes, require('../modules/auth')._routes);
Hoek.merge(allRoutes, require('../modules/users')._routes);
Hoek.merge(allRoutes, require('../modules/items')._routes);

// routes
const routes = (server) => allRoutes

module.exports = routes