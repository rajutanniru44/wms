'use strict'

const db = {
        "development": {
            "url": "mongodb://localhost:27017/test"
        },
        "production": {
            "url": "mongodb://localhost/test"
        }    
}

const cors = {
    // Allow consumption by any host
    origin: ['*'],
    // Allows authentication to be sent by default
    credentials: 'true',
    // These headers can be consumed
    exposedHeaders: ['content-type', 'content-length', 'api-version'],
    // For caching
    maxAge: 600,
    // These request headers can be sent
    headers: ['Accept', 'Content-Type', 'Authorization', 'api-version']
}



module.exports = {db: db.development, cors:cors}